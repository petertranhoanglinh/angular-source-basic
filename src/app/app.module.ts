import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {  HttpClientModule } from '@angular/common/http';
import { IntroModule } from './core/pages/intro.module';
import { ComponentsModule } from './core/components/components.module';
import { AdminModule } from './core/pages/admin/admin.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IntroModule,
    HttpClientModule,
    ComponentsModule,
    AdminModule
    
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
