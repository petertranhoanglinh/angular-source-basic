import { Component } from '@angular/core';
import { AuthDetail } from './core/common/auth-detail';
import { ValidationUtil } from './core/utils/validation.util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shop-web';
  show = '';
  close:boolean = false;
  changeCart:number = 0;

  isLogined:boolean = ValidationUtil.isNotNullAndNotEmpty(AuthDetail.getLoginedInfo());

  


  items = ['item1', 'item2', 'item3', 'item4'];

  addItem(newItem: string) {
    this.items.push(newItem);
  }

  showC():void{
    this.changeCart = Math.random();
    if(this.show == 'none'){
      this.show = '';
    }else{
      this.show= 'none';
    }
  }
  handelCloseCart(event: boolean ):void{
    this.close = event;
    this.show = '';
  }

  logOut(){
    AuthDetail.actionLogOut();
    window.location.href = "/login";
  }



}
