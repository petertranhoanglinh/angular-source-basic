import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const introModule = () => import ("src/app/core/pages/intro.module").then(x => x.IntroModule);
const adminModule = () => import ("src/app/core/pages/admin/admin.module").then(x => x.AdminModule);
import { LoginPageComponent } from './core/pages/login-page/login-page.component';
import { AuthGuardService } from './core/services/auth-guard.service';
const routes: Routes = [
  { path: '', loadChildren: introModule},
  { path: 'admin', canActivate:[AuthGuardService] ,loadChildren: adminModule},
  { path: 'login', component:LoginPageComponent}
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
