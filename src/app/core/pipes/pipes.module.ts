import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetCommaPipe } from './set-comma.pipe';


@NgModule({
  declarations: [
    SetCommaPipe,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    SetCommaPipe,
  ]
})
export class PipesModule { }
