
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: "setComma" })
export class SetCommaPipe implements PipeTransform {

  constructor() {}

  transform(value: String | Number | string | number | undefined): String {
    if (!value) {
      return "0";
    }
    return this.setComma(value);
  }

   setComma(value: String | Number): string {
    if (typeof value !== "undefined" && value !== null) {
      value = value.toString();
      let result = value + "";
      let regex = /(^[+-]?\d+)(\d{3})/;
      while (regex.test(result)) {
        result = result.replace(regex, '$1' + ',' + '$2');
      }
      return result;
    }
    return "0";
  }
}