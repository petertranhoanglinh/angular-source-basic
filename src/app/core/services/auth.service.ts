import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { AuthModel } from "../models/auth.model";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ValidationUtil } from "../utils/validation.util";
import { catchError, throwError } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class AuthService{
    res:string = '';
    constructor(private _http: HttpClient) { }

  auth(params:any){
    let url = `${environment.api}/user/auth`;
    return this._http.post<AuthModel>(url,params).pipe(
        catchError(err => {
            alert('Password or UserName is correct');
            return throwError(err);
        })
    );
  }

  checkJwt(jwt:string): boolean {
    let url = `${environment.api}checkLogin/${jwt}`;
    this._http.get<String>(url).subscribe(
      res => {
        if(ValidationUtil.isNotNullAndNotEmpty(res)) {
          this.res = String(res);
        }
      })
      if(this.res!= ""){
        return this.res=="true"?true:false
      }else{this.res = "false"}{
        return false;
      }
     }
    }
  

