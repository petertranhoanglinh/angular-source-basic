import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ProductModel } from '../models/product.model';
import { ResultProcModel } from '../models/result.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private _http: HttpClient) { }

  getProduct(pdtName:string, kind:string) {
    if(pdtName === ""){
        pdtName = "*";
    }
    if(kind === ""){
        kind = "*";
    }
    let url = `${environment.api}/product/getProduct/${pdtName}/${kind}`;
    return this._http.get<any[]>(url).pipe(
    );
  }

  getPdtKind(){
    let url = `${environment.api}/product/getPdtKind`;
    return this._http.get<any[]>(url).pipe(
    );
  }


  getPdt(pdtCd:string){
    let url = `${environment.api}/product/getPdt/${pdtCd}`;
    return this._http.get<any>(url).pipe(
    );
  }

  getApiProduct(params:any){
    let url = `${environment.api}/product/getProduct/*`;
    return this._http.get<ProductModel[]>(url).pipe();
  }

  getApiCountProduct(params:any){
    let url = `${environment.api}/product/countProduct`;
    return this._http.post<number>(url,params).pipe();
  }

  getApiCatelogy(){
    let url = `${environment.api}/product/getCatelogy`;
    return this._http.get<ProductModel[]>(url).pipe();
  }

  deleteProduct(productId:string){
    let url = `${environment.api}/product/deleteProduct/${productId}`;
    return this._http.get<ResultProcModel>(url).pipe();

  }

  addProduct(params:any){
    let url = `${environment.api}/product/addProduct`;
    return this._http.post<ResultProcModel>(url,params).pipe();
  }

  getSeqProduct(){
    
  }

  



}

