import { Injectable } from '@angular/core';
import { CartItem } from '../models/cart.model';
import { ValidationUtil } from '../utils/validation.util';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CartService{
  static cartItems: CartItem[] = [];

  constructor(private _http: HttpClient) { 
    
  }
  

  static addToCart(item:CartItem){
    this.mapCartItems();
    let length = this.cartItems.length;
    if(length > 0){
      for (let index = 0; index < length; index++) {
        const element = this.cartItems[index].productId;
        if(element == item.productId){
          const qty = this.cartItems[index].qty;
          // remove item 
          this.cartItems = this.cartItems.filter((e, index) => e.productId !== item.productId);
          this.cartItems.push({productId:item.productId , qty:qty+1, vat:item.vat,imageCart:item.imageCart,productPrice:item.productPrice, productName:item.productName})
        }

        if(element != item.productId && index == this.cartItems.length - 1){
          this.cartItems.push({productId:item.productId , qty:1, vat:item.vat,imageCart:item.imageCart,productPrice:item.productPrice, productName:item.productName})
        
      }
    }
    }else{
      this.cartItems.push({productId:item.productId, qty:1, vat:item.vat,imageCart:item.imageCart,productPrice:item.productPrice, productName:item.productName})
    }
     
    window.localStorage.setItem("CART_ITEMS",JSON.stringify(this.cartItems));
  }

  static gerCartItems(): CartItem[]{
    let str = localStorage.getItem("CART_ITEMS");
    if (ValidationUtil.isNotNullAndNotEmpty(str)) {
      return JSON.parse(str + "");
    }
    return [];
  }

  static mapCartItems(){
    let str = localStorage.getItem("CART_ITEMS");
    if (ValidationUtil.isNotNullAndNotEmpty(str)) {
      this.cartItems = JSON.parse(str + "");
    }else{
      this.cartItems = [];
    }
    
  }

  static clearCart(){
    localStorage.removeItem('CART_ITEMS');
  }

  static removeItem(productId:string){
    this.mapCartItems();
    let length = this.cartItems.length;
    if(length > 0){
      for (let index = 0; index < length; index++) {
        const element = this.cartItems[index].productId;
        if(element == productId){
          // remove item 
          this.cartItems = this.cartItems.filter((e, index) => e.productId !== productId);
          break;
        }
      }
    }
    window.localStorage.setItem("CART_ITEMS",JSON.stringify(this.cartItems));
  }

  checkout(params:any){
    let url = `${environment.api}/createOrder`;
    return this._http.post<any[]>(url,params).pipe(
    );
  }
}
