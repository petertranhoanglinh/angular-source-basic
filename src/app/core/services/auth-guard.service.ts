import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';
import { ValidationUtil } from '../utils/validation.util';
import { AuthDetail } from '../common/auth-detail';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  isLogin:boolean = false;
  constructor(private _router: Router, private _authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
 
      
     // const isLogined = this._authService.checkJwt(ConvertUtil.convertToSring(AuthDetail.getLoginedInfo()?.jwt));
      if(!ValidationUtil.isNotNullAndNotEmpty(AuthDetail.getLoginedInfo()?.jwt)){
        this._router.navigate(["/login"]);
      }

      return true;
  }
}
