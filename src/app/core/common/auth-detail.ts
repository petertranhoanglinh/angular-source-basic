
import { HttpHeaders } from "@angular/common/http";
import { ValidationUtil } from "../utils/validation.util";
import { AuthModel } from "../models/auth.model";

export class AuthDetail{
  static _authService: any;
  constructor(){
  }
    static getLoginedInfo(): AuthModel | null {
        let str = localStorage.getItem("member");
        if (ValidationUtil.isNotNullAndNotEmpty(str)) {
          return JSON.parse(str + "");
        }
        return null;
    }

    static actionLogOut(){
      localStorage.removeItem('member');
      localStorage.removeItem('lastAction');
      setTimeout(()=>{
        console.log("Your Session Expired due to longer Inactivity, Login Again To Continue");
      },10000);
      
    }

    static getHeaderJwt():any{
      return new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${AuthDetail.getLoginedInfo()?.jwt}`
      });
   }

   

    
}