import { Component, OnInit } from '@angular/core';
import { CartItem } from '../../models/cart.model';
import { CartService } from '../../services/cart.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cartItems : CartItem [] = CartService.gerCartItems();
  totalPrice:number = 0;
  fullName:string = '';
  address:string = '';
  email:string = '';
  mobile:string = '';
  city:string = '';
  state:string = '';
  county:string = '';
  zip:string='';
  product = new Map<string, number>();

   checkoutForm = this.fb.group({
     shipping: this.fb.group({
       name: ["", [Validators.required]],
       mobile: ["", [Validators.required]],
       email: [""],
       post: ["", [Validators.required]],
       addr1: ["", [Validators.required]],
       addr2: ["", [Validators.required]],
       addr3: ["", [Validators.required]],
       addrDeteil: ["", [Validators.required]],
     }),
     bank: this.fb.group({
       cardCd: ["", [Validators.required]],
       cardNo: ["", [Validators.required]],
       cardHolder: ["", [Validators.required]],
       cardAmt: ["", [Validators.required]],
       regDate: ["", [Validators.required]],
     }),


    
  });

  constructor(private fb: FormBuilder,) { 
    
  }

  ngOnInit(): void {
    this.getCartItem();
  }

  getTotalPrice():void{
    this.totalPrice = 0;
    for (let index = 0; index < this.cartItems.length; index++) {
      this.totalPrice = this.totalPrice + (this.cartItems[index].qty * this.cartItems[index].productPrice);
      this.product.set(this.cartItems[index].productId.toString(),this.cartItems[index].qty);
    }
  }
  getCartItem():void{
    this.cartItems = CartService.gerCartItems();
    this.getTotalPrice();
  }

  getParams():any{
    const params = {
      "userid":"amouse",
      "fullname":this.fullName,
      "mobile":this.mobile,
      "email":this.email,
      "address":this.address,
      "city":this.city,
      "county":this.county,
      "state":this.state
    }
  }

  createOrder():void{

  }

  
}
