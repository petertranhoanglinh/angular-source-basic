import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckoutComponent } from '../../pages/checkout/checkout.component';
import { PipesModule } from '../../pipes/pipes.module';
import { ShippingComponent } from './shipping/shipping.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
@NgModule({
  declarations: [CheckoutComponent,ShippingComponent],
  imports: [
    CommonModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CheckoutModule { }
