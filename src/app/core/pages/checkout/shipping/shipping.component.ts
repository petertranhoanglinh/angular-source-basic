import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['../checkout.component.css']
})

export class ShippingComponent implements OnInit {
  @Input() 
  formGroupName: string = "";
  
  form: FormGroup = {} as FormGroup;
  constructor( private _rootFormGroup: FormGroupDirective,) { }

  ngOnInit(): void {

    this.form = this._rootFormGroup.control.get(this.formGroupName) as FormGroup;
  }

}
