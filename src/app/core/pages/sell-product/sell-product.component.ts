import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ProductService } from 'src/app/core/services/product.service';
import { environment } from 'src/environments/environment';
import { ProductModel } from '../../models/product.model';
import { CartItem } from '../../models/cart.model';
import { CartService } from '../../services/cart.service';
import { ValidationUtil } from '../../utils/validation.util';
@Component({
  selector: 'app-sell-product',
  templateUrl: './sell-product.component.html',
  styleUrls: ['./sell-product.component.css']
})
export class SellProductComponent implements OnInit {

 // @Output() newItemEvent = new EventEmitter<string>();
  page:number = 0;
  len:number = 8;
  data:ProductModel[] = [];
  pdtKind:any[] = [];
  image:string = '';
  active:string = '';
  totalProduct:number = 0;
  cartItems: CartItem[] = [];
  item: CartItem = {} as CartItem;
  catelogy:ProductModel[] = [];
  cateId:number = 0;
  productId:number = 0;


  
  constructor(
    private _productService:ProductService
  ) { }

  ngOnInit(): void {
   this.getProduct('','');
   //this.getPdtKind();
   this.getCountProduct();
   this.getCartItems();
   this.getCatelogy();

  }

  getProduct(pdtName:string, kind:string):void{
    this.cateId = Number(kind);
    //this.productId = Number(pdtName)
    this._productService.getApiProduct(this.getParams()).subscribe(res =>{
      if(res != undefined || res != null){
        this.data = res
      }
    })
    this.active = kind;
  }

  getPdtKind():void{
    this._productService.getPdtKind().subscribe(res =>{
      this.pdtKind = res;
    })
  }
  addAPI(data:any):string{
    return environment.api.replace('api' , '') + data;
  }

  toString(data:any):string{
    return data.toString();
  }
  // addNewItem(value: string) {
  //   this.newItemEvent.emit(value);
  // }


  getParams():any{
    const params={
      "min":this.page,
      "max":this.len,
      "cateId":this.cateId,
      "productId":this.productId
    }

    return params;
  }

  getCountProduct():void{
    this._productService.getApiCountProduct(this.getParams()).subscribe(
      res=>{
        if(res != undefined || res != null){
          this.totalProduct = res
        }
      }
    )
  }

  clickPage(page:number):void{
    this.page = page - 1;
    this.getProduct('',String(this.cateId));
  }

  addToCart(product:ProductModel){
    this.item = {productId:product._id,imageCart:product.image,productPrice:product.price ,  
      productName:product.name, vat:0, qty:1}
    alert('add to cart suscess');
    CartService.addToCart(this.item);
    if(ValidationUtil.isNotNullAndNotEmpty(this.cartItems)){
      
      this.getCartItems();
    }
   
  }

  getCartItems():void{
    this.cartItems = CartService.gerCartItems();
   
  }

  getCatelogy():void{
    this._productService.getApiCatelogy().subscribe(
      res =>{
        if(ValidationUtil.isNotNullAndNotEmpty(res)){
          this.catelogy = res;
        }
      }
    )
  }

}
