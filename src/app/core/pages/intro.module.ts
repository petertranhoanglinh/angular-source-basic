import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroRoutingModule } from './intro-routing.module';
import { IntroComponent } from './intro.component';
import { HomePageComponent } from './home-page/home-page/home-page.component';
import { AboutComponent } from './about/about.component';
import { FeaturedProductsComponent } from './featured-products/featured-products.component';
import { SellProductComponent } from './sell-product/sell-product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { CartComponent } from './cart/cart.component';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';
import { CheckoutModule } from './checkout/checkout.module';
import { LoginPageComponent } from './login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { FashSaleComponent } from './fash-sale/fash-sale.component';
import { BrandComponent } from './brand/brand.component';

@NgModule({
  declarations: [IntroComponent,HomePageComponent , AboutComponent ,FeaturedProductsComponent, SellProductComponent, ProductDetailComponent, CartComponent, LoginPageComponent, FashSaleComponent, BrandComponent],
  imports: [
    CommonModule,
    IntroRoutingModule,
    ComponentsModule,
    PipesModule,
    CheckoutModule,
    FormsModule
  ],
  exports: [IntroComponent , CartComponent]
})
export class IntroModule { }
