import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FashSaleComponent } from './fash-sale.component';

describe('FashSaleComponent', () => {
  let component: FashSaleComponent;
  let fixture: ComponentFixture<FashSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FashSaleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FashSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
