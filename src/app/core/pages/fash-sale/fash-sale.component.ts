import { Component, OnInit } from '@angular/core';
declare const fashSaleSwipper:any
@Component({
  selector: 'app-fash-sale',
  templateUrl: './fash-sale.component.html',
  styleUrls: ['./fash-sale.component.css']
})
export class FashSaleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    fashSaleSwipper();
  }

}
