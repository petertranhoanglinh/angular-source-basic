import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { WiewProductComponent } from './wiew-product/wiew-product.component';
@NgModule({
  declarations: [AdminComponent, DashboardComponent, AccountComponent, AddProductComponent, EditProductComponent, WiewProductComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
})
export class AdminModule { }
