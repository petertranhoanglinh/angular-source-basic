import { Component, OnInit } from '@angular/core';
import { ProductModel } from 'src/app/core/models/product.model';
import { ResultProcModel } from 'src/app/core/models/result.model';
import { ProductService } from 'src/app/core/services/product.service';
import { ValidationUtil } from 'src/app/core/utils/validation.util';

@Component({
  selector: 'app-wiew-product',
  templateUrl: './wiew-product.component.html',
  styleUrls: ['./wiew-product.component.css']
})
export class WiewProductComponent implements OnInit {
  product:ProductModel[] = [];
  cate:ProductModel[] = [];
  result:ResultProcModel = {} as ResultProcModel;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {

    this.getProduct();
    this.productService.getApiCatelogy().subscribe(res =>{
      if(ValidationUtil.isNotNullAndNotEmpty(res)){
        this.cate = res;
      }
    })
  }

  deleteProduct(pdtCd:string){
    this.productService.deleteProduct(String(pdtCd)).subscribe( res =>{
      if(ValidationUtil.isNotNullAndNotEmpty(res)){
          alert(res.retCode);
          this.getProduct();
      }
    })
  }

  getProduct(){
    this.productService.getApiProduct(this.getParams()).subscribe(res =>{

      if(ValidationUtil.isNotNullAndNotEmpty(res)){
        this.product = res;
      }


    })

  }

  getParams():any{
    const params={
      "min":0,
      "max":1000,
      "cateId":0,
      "productId":0
    }

    return params;
  }

}
