import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WiewProductComponent } from './wiew-product.component';

describe('WiewProductComponent', () => {
  let component: WiewProductComponent;
  let fixture: ComponentFixture<WiewProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WiewProductComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WiewProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
