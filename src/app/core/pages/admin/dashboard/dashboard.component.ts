import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthDetail } from 'src/app/core/common/auth-detail';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  username:string = '';
  constructor() { }

  ngOnInit(): void {

    this.username = String(AuthDetail.getLoginedInfo()?.username)
  }

}
