import { Component, OnInit } from '@angular/core';
import { AuthDetail } from '../../common/auth-detail';
import { ValidationUtil } from '../../utils/validation.util';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  activeId:string = 'dash';

  constructor() { }

  ngOnInit(): void {

    // if(ValidationUtil.isNotNullAndNotEmpty(AuthDetail.getLoginedInfo())){
    //   return
    // }else{
    //   location.href ="/login";
    // }

  }

  changeActiveId(kind:string){
    this.activeId = kind;

  }

  logout(){
    AuthDetail.actionLogOut();
    location.href = "/";
  }

}
