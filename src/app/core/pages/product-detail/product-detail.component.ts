import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { environment } from 'src/environments/environment';
import { ProductModel } from '../../models/product.model';
import { CartService } from '../../services/cart.service';
import { ValidationUtil } from '../../utils/validation.util';
import { CartItem } from '../../models/cart.model';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  pdtCd!:any;
  data: ProductModel = {} as ProductModel;
  urlApi:string = environment.api.replace('api' , '');
  cartItems: CartItem[] = [];
  item: CartItem = {} as CartItem;
  constructor(private _activatedRoute: ActivatedRoute, private _productService:ProductService){

    
  }

  ngOnInit(): void {
    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.pdtCd = params.get("pdtCd");
      this.getProduct(this.pdtCd.toString());
    }); 
  }

  getProduct(pdtCd:string):void{
    this._productService.getApiProduct(this.getParams()).subscribe(res =>{
      this.data = res[0];
    })
  }
  addToCart(product:ProductModel){
    alert('Add to cart suscess');
    this.item = {productId:product._id,imageCart:product.image,productPrice:product.price ,  
      productName:product.name, vat:0, qty:1}
    CartService.addToCart(this.item);
    if(ValidationUtil.isNotNullAndNotEmpty(this.cartItems)){
      
      this.getCartItems();
    }
   
  }

  getCartItems():void{
    this.cartItems = CartService.gerCartItems();
   
  }

  getParams():any{
    const params={
      "min":0,
      "max":10,
      "cateId":0,
      "productId":this.pdtCd
    }

    return params;
  }


  

}
