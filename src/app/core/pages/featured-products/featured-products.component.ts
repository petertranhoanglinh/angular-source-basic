import { Component, OnInit } from '@angular/core';
declare const featuredSwipper:any;
@Component({
  selector: 'app-featured-products',
  templateUrl: './featured-products.component.html',
  styleUrls: ['./featured-products.component.css']
})
export class FeaturedProductsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    featuredSwipper();
  }

}
