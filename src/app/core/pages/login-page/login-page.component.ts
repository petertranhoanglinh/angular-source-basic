import { Component, OnInit } from '@angular/core';
import { AuthDetail } from '../../common/auth-detail';
import { AuthModel } from '../../models/auth.model';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ValidationUtil } from '../../utils/validation.util';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  email:string ='';
  password:string = '';
  vali:string = '';

  animal: string = '';
  name: string = '';
  userAuth$ = new Observable<AuthModel>();
  userAuth:AuthModel = {} as AuthModel;
  clearAuth$ = {} as Subscription;
  
  constructor(private _router: Router,
    private _authService: AuthService){
  }

  ngOnInit(): void {
    AuthDetail.actionLogOut();
  }
  loginPage():void{ 
    this._authService.auth(this.getParams()).subscribe(
    res =>{
       this.loginProcess(res);
    })
    
    
  }



  loginProcess(res: AuthModel) {
    localStorage.setItem('member', JSON.stringify(res));
    sessionStorage.setItem('username', JSON.stringify(res.username));
    if(ValidationUtil.isNotNullAndNotEmpty(res.username)){
      location.href ="/";
    }
  }

  getParams(): any {
    let params = {
      email:  this.email,
      password:this.password
    }
    return params;
  }


}
