import { Component, OnInit } from '@angular/core';
import { ValidationUtil } from '../utils/validation.util';
import { AuthDetail } from '../common/auth-detail';
 declare const initMenu:any
@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {
  isLogined:boolean =false; 
  constructor() { }

  ngOnInit(): void {
    initMenu();
    this.isLogined =  ValidationUtil.isNotNullAndNotEmpty(AuthDetail.getLoginedInfo())
  }

  items = ['item1', 'item2', 'item3', 'item4'];

  logOut(){
    AuthDetail.actionLogOut();
    window.location.href = "/login";
  }

}
