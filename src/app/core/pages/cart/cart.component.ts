import { Component, EventEmitter, HostListener, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { CartItem } from '../../models/cart.model';
import { ValidationUtil } from '../../utils/validation.util';
import { CartService } from '../../services/cart.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  @Input() 
  close:boolean = true;

  @Input() 
  changeCart:number = 0;

  @Output()
  onCloseCart =  new EventEmitter<boolean>();

  cartItems : CartItem [] = CartService.gerCartItems();
  constructor(private _router: Router, 
              private _cartService:CartService) { }

  urlApi:string = environment.api.replace('api' , '');

  totalPrice:number = 0;
  listProduct = new Map<string, number>();

  ngOnChanges(changes: SimpleChanges): void {
    let changeCart = changes['changeCart'];
    this.getCartItem();
  }


  ngOnInit(): void {
    this.getCartItem();

   
  }

  onClickClose():void{
    this.onCloseCart.emit(true)
  }

  removeItem(productId:string){
    CartService.removeItem(productId);
    this.getCartItem();
  }

  removeAllCart():void{
    CartService.clearCart();
    this.getCartItem();
  }

  getCartItem():void{
    this.cartItems = CartService.gerCartItems();
    this.getTotalPrice();
  }

  getTotalPrice():void{
    this.totalPrice = 0;
    for (let index = 0; index < this.cartItems.length; index++) {
      this.totalPrice = this.totalPrice + (this.cartItems[index].qty * this.cartItems[index].productPrice);
    }
  }
  getPrams():any{
    this.listProduct.clear();
    this.getCartItem();
    for (let i = 0; i < this.cartItems.length; i++) {
      this.listProduct.set(this.cartItems[i].productId,this.cartItems[i].qty)
    }

    let params ={
      
      "listProduct":this.listProduct
    }
    
  }



  checkout():void{
    this.onClickClose();
    if(ValidationUtil.isNullOrEmpty(this.cartItems)){
      alert('Không có sản phẩm nào trong giỏ hàng');
      return;
    }
    this._router.navigate(['/checkout']);

  }

  toString(data:any):string{
    return data.toString();
  }

  clickLinkProduct(productId:string){
    window.location.href = "/detail-product/"+productId;
  }

  

}
