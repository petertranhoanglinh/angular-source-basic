import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntroComponent } from './intro.component';
import { AboutComponent } from './about/about.component';
import { FeaturedProductsComponent } from './featured-products/featured-products.component';
import { SellProductComponent } from './sell-product/sell-product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { HomePageComponent } from './home-page/home-page/home-page.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { FashSaleComponent } from './fash-sale/fash-sale.component';
import { BrandComponent } from './brand/brand.component';
const routes: Routes = [
  { 
    path: '', component: IntroComponent, children: [
      { path: '', component: HomePageComponent },
      { path: 'about', component: AboutComponent },
      { path: 'feature-product', component: FeaturedProductsComponent },
      { path: 'fash-sale', component: FashSaleComponent },
      { path: 'sell-product', component: SellProductComponent },
      { path: 'detail-product/:pdtCd', component: ProductDetailComponent },
      { path: 'brand', component: BrandComponent },
      { path: 'checkout', component:CheckoutComponent}
    ] 
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntroRoutingModule {}
