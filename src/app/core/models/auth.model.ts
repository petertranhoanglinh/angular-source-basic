export interface AuthModel{
    _id:String ;
    username:String ;
    email:String;
    role:String;
    createAt:String;
    updateAt:String;
    jwt:String;
}

