export interface CartItem{
    qty:number;
    productId:string;
    productPrice:number;
    vat:number;
    imageCart:string;
    productName:string;
}