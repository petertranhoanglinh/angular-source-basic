export interface ProductModel{
    _id:string;
    name:string;
    quantity:string;
    price:number;
    image:string;
    cateName:string
}