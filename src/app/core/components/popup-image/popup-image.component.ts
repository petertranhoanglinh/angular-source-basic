import { Component, OnInit } from '@angular/core';
declare const popupImage:any;
@Component({
  selector: 'app-popup-image',
  templateUrl: './popup-image.component.html',
  styleUrls: ['./popup-image.component.css']
})


export class PopupImageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    popupImage();
  }

}
