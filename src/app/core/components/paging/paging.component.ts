import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.css']
})
export class PagingComponent implements OnInit {

  @Input() len:number = 0;
  @Input() totalItem:number = 0;
  listPage:number[] = [1,2,3,4,5];
  lengthPage:number = 0;
  listTempPage:number [] = [];
  pageActive:number = 1;

  

  @Output() handelClickPage = new EventEmitter<number>();

  constructor() { }
  ngOnInit(): void {
    this.init();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let totalItem = changes["totalItem"];
    this.init();
  }
  init():void{
    this.listPage = [];
    if(this.totalItem % this.len == 0){
      this.lengthPage = this.totalItem / this.len;
    }else{
      let phan_nguyen = (this.totalItem - (this.totalItem % this.len));
      this.lengthPage = (phan_nguyen / this.len)+1;
    }

    for(let i = 0 ; i < this.lengthPage ; i++){
      if(i < 5){
        this.listTempPage.push(i+1)
      }
      this.listPage.push(i+1);
    }
  }

  clickSubPage(page:number):void{
      this.handelClickPage.emit(page);
      this.pageActive = page;
      this.listTempPage = [];
      let a = 2;
      let b = 2;
      if(page == 1){
        a= 0;
        b = 4;
      }
      if(page == 2){
        a=1;
        b=3; 
      }
      if(page == this.lengthPage - 1){
       a = 3;
       b = 1;
      }

      if(page == this.lengthPage){
        a = 4;
        b=0;
      }
      if(page <= this.lengthPage){
        for (let index = 1; index <= this.lengthPage; index++) {
          if(index >= page - a && index <= page+b){
            this.listTempPage.push(index);
          }
        }
      }

  }

}
