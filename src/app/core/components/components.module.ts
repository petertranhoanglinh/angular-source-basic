import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagingComponent } from './paging/paging.component';
import { PopupImageComponent } from './popup-image/popup-image.component';



@NgModule({
  declarations: [
    PagingComponent,
    PopupImageComponent
  ],
  imports: [
    CommonModule
  ],

  exports:[
    PagingComponent,
    PopupImageComponent
  ]
})
export class ComponentsModule { }
